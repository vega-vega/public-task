<?php


class Controller
{
    /** @var Manager */
    private $manager;

    public function __construct()
    {
        $this->manager = new Manager();
    }

    /**
     * @param int $page
     * @param string $sort
     */
    function main_index($page, $sort)
    {
        $response = $this->manager->get_task_list($page, $sort);
        $response->setIsAuth($this->hash_is_valid());

        View::main_page($response);
    }

    function login_index() {
        $response = new Task_Response();
        if (!$this->hash_is_valid()) {
            $is_valid = $this->check_authorization_valid($response);
        } else {
            $is_valid = true;
        }

        if ($is_valid) {
            $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
            header('Location:' . $host );
        }

        View::login_page($response);
    }

    function logout_index() {
        unset($_SESSION['session_hash']);
        $this->manager->clear_hash();
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('Location:' . $host );
    }

    function error_index()
    {
        View::error_page();
    }

    function task_create_index()
    {
        $response = new Task_Response();
        $response->setIsAuth($this->hash_is_valid());
        $is_done = false;

        if (isset($_POST['task_edit'])) {
            $id = $_POST['task_edit']['id'];
            $this->manager->load_task($id);
        } elseif (isset($_POST['task_form'])) {
            $form = $_POST['task_form'];
            $err = $this->manager->create_task($form, $response->isIsAuth());
            $response->setError($err);

            $is_done = count($err) < 1;
        }

        if ($is_done) {
            $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
            header('Location:' . $host );
        }

        View::task_edit_page($response);
    }

    /**
     * @param integer $page
     * @param integer $id
     */
    function task_done_index($page, $id)
    {
        if ($this->hash_is_valid()) {
            $this->manager->done_task($id);
        }

        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('Location:' . $host . '/' . $page);
    }

    /**
     * @param Task_Response $response
     * @return bool
     */
    private function check_authorization_valid($response) {
        if (isset($_POST['authorization'])) {
            $login = $_POST['authorization']['login'];
            $password = $_POST['authorization']['password'];

            $hash = $this->manager->login($login, $password);

            if (!is_null($hash)) {
                $_SESSION['session_hash'] = $hash;
                $response->setIsInvalidAuthorization(true);
                return true;
            }

            $response->setError(["Не вернные данные."]);
        }

        $response->setIsInvalidAuthorization(true);
        return false;
    }

    /**
     * @return bool
     */
    private function hash_is_valid() {
        if (array_key_exists('session_hash', $_SESSION)) {
            $hash = $_SESSION['session_hash'];
            return $this->manager->check_hash($hash);
        }

        return false;
    }
}