<?php


class View
{
    /**
     * @param Task_Response $response
     */
    static function main_page($response)
    {
        include 'public/page/main_page.php';
    }

    /**
     * @param Task_Response $response
     */
    static function login_page($response)
    {
        include 'public/page/login_page.php';
    }

    static function error_page()
    {
        include 'public/page/error_page.php';
    }

    /**
     * @param Task_Response $response
     */
    static function task_edit_page($response)
    {
        include 'public/page/task_edit_page.php';
    }

    static function get_status($status)
    {
        switch ($status) {
            case 1:
                return "Отредактировано администратором";
            case 2:
                return "Выполено";
            default:
                return "Не выполнено";
        }
    }
}