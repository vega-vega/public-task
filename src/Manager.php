<?php


class Manager
{
    /**
     * @param string $login
     * @param string $password
     * @return string
     */
    function login($login, $password)
    {
        $mysqli = $this->connect_mysqli();

        $login = $mysqli->real_escape_string($login);
        $user_id = null;
        $is_valid = false;
        if ($result = $mysqli->query("SELECT id, password FROM user WHERE login = '$login'", MYSQLI_USE_RESULT)) {
            $row = $result->fetch_row();

            $user_id = $row[0];
            $is_valid = password_verify($password, $row[1]);
            $result->close();
        }

        $hash = null;
        if ($is_valid) {
            $hash = $this->create_hash($mysqli, $user_id);
        }

        $mysqli->close();

        return $hash;
    }

    /**
     * @param string $hash
     * @return bool
     */
    function check_hash($hash)
    {
        $is_valid = false;
        $mysqli = $this->connect_mysqli();

        if ($result = $mysqli->query("SELECT id FROM user WHERE hash = '$hash'")) {
            $is_valid = $result->num_rows > 0;
        }

        $mysqli->close();

        return $is_valid;
    }

    /**
     * @param integer $page
     * @param string $sort
     * @return Task_Response
     */
    function get_task_list($page, $sort)
    {
        $limit = ($page - 1) * 3;
        $query = "SELECT id, status, fix, name, email, text FROM task " . $this->get_order($sort) . " LIMIT $limit, 3;";
        $response = new Task_Response();

        $mysqli = $this->connect_mysqli();

        if ($result = $mysqli->query($query, MYSQLI_USE_RESULT))
        {
            while ($row = $result->fetch_assoc())
            {
                $task = new Task($row);
                $response->add_task($task);
            }

            $result->close();
        }

        if ($result = $mysqli->query("SELECT COUNT(id) FROM task;", MYSQLI_USE_RESULT))
        {
            $row = $result->fetch_row();
            $count = ceil($row[0] / 3);

            $response->setTaskCount($count);
        }

        $mysqli->close();

        return $response;
    }

    /**
     * @param array $form
     * @param bool $is_admin
     * @return array
     */
    function create_task($form, $is_admin)
    {
        $err = [];
        $name = htmlspecialchars($form['name']);
        $email = htmlspecialchars($form['email']);
        $text = htmlspecialchars($form['text']);

        if (!isset($name)) {
            $err['name'] = 'Необходимое поле.';
        }

        if (!isset($email)) {
            $err['email'] = 'Необходимое поле.';
        } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $err['email'] = 'Не верный формат почты.';
        }

        if (!isset($text)) {
            $err['text'] = 'Необходимое поле.';
        }

        if (count($err) > 0) {
            return $err;
        }

        if (!$is_admin) {
            return $this->insert_task($name, $email, $text);
        } else {
            $id = $form['id'];
            if (is_null($id)) {
                return $this->insert_task($name, $email, $text);
            }

            return $this->update_task($id, $name, $email, $text);
        }
    }

    /**
     * @param $id
     */
    function load_task($id)
    {
        $mysqli = $this->connect_mysqli();
        if ($result = $mysqli->query("SELECT id, status, fix, name, email, text FROM task WHERE id = $id;", MYSQLI_USE_RESULT)) {
            $row = $result->fetch_assoc();
            $task = new Task($row);

            $_POST['task_form']['id'] = $task->getId();
            $_POST['task_form']['name'] = $task->getName();
            $_POST['task_form']['email'] = $task->getEmail();
            $_POST['task_form']['text'] = $task->getText();
        }
        $mysqli->close();
    }

    /**
     * @param $id
     */
    function done_task($id) {
        $mysqli = $this->connect_mysqli();
        $mysqli->query("UPDATE task SET status = 1 WHERE id = $id");
        $mysqli->close();
    }

    function clear_hash() {
        try {
            $hash = bin2hex(random_bytes(15));
            $mysqli = $this->connect_mysqli();
            $mysqli->query("UPDATE user SET hash = '$hash'");
            $mysqli->close();
        } catch (Exception $e) {
            printf($e->getMessage());
            exit();
        }
    }

    /**
     * @param string $sort
     * @return string
     */
    private function get_order($sort)
    {
        switch ($sort) {
            case 'name':
                return " ORDER BY name ";
            case 'name_desc':
                return " ORDER BY name desc ";
            case 'email':
                return " ORDER BY email";
            case 'email_desc':
                return " ORDER BY email desc ";
            case 'status':
                return " ORDER BY status ";
            case 'status_desc':
                return " ORDER BY status desc ";
            default:
                return " ";
        }
    }

    /**
     * @return Mysqli
     */
    private function connect_mysqli()
    {
        $config_path = "./config.json";
        if (!file_exists($config_path)) {
            printf("Нет конфигурационного файла.");
            exit();
        }

        $content = file_get_contents("./config.json");
        $config = json_decode($content, true);

        $mysqli = new mysqli($config['server'], $config['user'], $config['password'], $config['database']);

        if (mysqli_connect_errno()) {
            printf("Не удалось подключиться: %s\n", mysqli_connect_error());
            exit();
        }

        return $mysqli;
    }

    /**
     * @param Mysqli $mysqli
     * @param int $id
     * @return string
     */
    private function create_hash($mysqli, $id)
    {
        try {
            $hash = bin2hex(random_bytes(15));
            $mysqli->query("UPDATE user SET hash = '$hash' WHERE id = '$id';");
        } catch (Exception $e) {
            printf($e->getMessage());
            exit();
        }

        return $hash;
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $text
     * @return array
     */
    private function insert_task($name, $email, $text)
    {
        $err = [];
        $mysqli = $this->connect_mysqli();

        $name = $mysqli->real_escape_string($name);
        $email = $mysqli->real_escape_string($email);
        $text = $mysqli->real_escape_string($text);

        if (!$mysqli->query("INSERT into task (name, email, text) VALUES ('$name', '$email', '$text')"))
        {
            $err['sql'] = $mysqli->error;
        }

        $mysqli->close();

        return $err;
    }

    /**
     * @param integer $id
     * @param string $name
     * @param string $email
     * @param string $text
     * @return array
     */
    private function update_task($id, $name, $email, $text)
    {
        $err = [];
        $mysqli = $this->connect_mysqli();

        $name = $mysqli->real_escape_string($name);
        $email = $mysqli->real_escape_string($email);
        $text = $mysqli->real_escape_string($text);

        if (!$mysqli->query("UPDATE task SET fix = 1, name = '$name', email = '$email', text = '$text' WHERE id = $id;"))
        {
            $err['sql'] = $mysqli->error;
        }

        $mysqli->close();

        return $err;
    }
}