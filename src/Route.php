<?php


class Route
{
    static function start()
    {
        $controller = new Controller();

        $routes = explode('/', $_SERVER['REQUEST_URI']);
        $path = $routes[1];
        $path = strlen($path) < 1 ? 1 : $path;

        if (is_numeric($path)) {
            $sort = null;
            if (!empty($routes[2])) {
                 $sort = $routes[2];
            }

            $controller->main_index($path, $sort);
        } elseif ($path == 'task') {
            $controller->task_create_index();
        } elseif ($path == 'done') {
            $id = $routes[2];
            $page = strlen($routes[3]) < 1 ? 1 : $routes[3];

            $controller->task_done_index($page, $id);
        } elseif ($path == 'login') {
            $controller->login_index();
        } elseif ($path == 'logout') {
            $controller->logout_index();
        } else {
            $controller->error_index();
        }
    }
}