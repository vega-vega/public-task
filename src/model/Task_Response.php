<?php


class Task_Response
{
    /** @var array[Task]  */
    private $task_arr;
    /** @var int  */
    private $task_count;
    /** @var bool  */
    private $is_auth;
    /** @var int  */
    private $is_valid_login;
    /** @var array */
    private $error;

    /**
     * Response constructor.
     */
    public function __construct()
    {
        $this->task_arr = [];
        $this->task_count = 0;
        $this->is_auth = false;
        $this->is_valid_login = false;
        $this->error = [];
    }


    /** @var Task */
    function add_task($task) {
        array_push($this->task_arr, $task);
    }

    /**
     * @return array[Task]
     */
    function getTaskArr()
    {
        return $this->task_arr;
    }

    /**
     * @return int
     */
    function getTaskCount()
    {
        return $this->task_count;
    }

    /**
     * @param int $task_count
     */
    function setTaskCount($task_count)
    {
        $this->task_count = $task_count;
    }

    /**
     * @return bool
     */
    function isIsAuth()
    {
        return $this->is_auth;
    }

    /**
     * @param bool $is_auth
     */
    function setIsAuth($is_auth)
    {
        $this->is_auth = $is_auth;
    }

    /**
     * @return int
     */
    function isValidLogin()
    {
        return $this->is_valid_login;
    }

    /**
     * @param int $is_invalid_login
     */
    function setIsInvalidAuthorization($is_invalid_login)
    {
        $this->is_valid_login = $is_invalid_login;
    }

    /**
     * @return array
     */
    function getError()
    {
        return $this->error;
    }

    /**
     * @param array $error
     */
    function setError($error)
    {
        $this->error = $error;
    }

}