<?php


class Task
{
    /** @var integer */
    private $id;
    /** @var integer */
    private $status;
    /** @var integer */
    private $fix;
    /** @var string */
    private $name;
    /** @var string */
    private $email;
    /** @var string */
    private $text;

    /**
     * @param array $row
     */
    public function __construct($row = null)
    {
        if (is_array($row) && count($row) == 6)
        {
            $this->id = $row['id'];
            $this->status = $row['status'];
            $this->fix = $row['fix'];
            $this->name = $row['name'];
            $this->email = $row['email'];
            $this->text = $row['text'];
        }
    }

    /**
     * @return int
     */
    function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    function getFix()
    {
        return $this->fix;
    }

    /**
     * @param int $fix
     */
    function setFix($fix)
    {
        $this->fix = $fix;
    }

    /**
     * @return string
     */
    function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    function setText($text)
    {
        $this->text = $text;
    }

}