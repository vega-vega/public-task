<!DOCTYPE html>
<?php include 'public/page/header_page.php'; ?>
<body>
<div class="container">
    <?php include 'public/page/navigation_page.php'; ?>
    <?php
        $name = "";
        $email = "";
        $text = "";

        $err = $response->getError();
        $name_err = isset($err['name']) ? $err['name'] : null;
        $email_err = isset($err['email']) ? $err['email'] : null;
        $text_err = isset($err['text']) ? $err['text'] : null;
        $is_send = false;

        if (isset($_POST['task_form'])) {
            $name = $_POST['task_form']['name'];
            $email = $_POST['task_form']['email'];
            $text = $_POST['task_form']['text'];
            $id = $_POST['task_form']['id'];
            $is_send = true;
        }
    ?>
    <div class="row">
        <div class= "col-md-offset-4 col-md-4">
            <form role="form" method="post" data-toggle="validator">
                <fieldset>
                    <input type="text"  name="task_form[id]" id="name" class="invisible" value="<?= $id ?>">
                    <p class="text-uppercase">Добавить задачу</p>
                    <div class="form-group <?= $is_send && isset($name_err) ? 'has-error' : ''?>">
                        <input type="text" data-error="Необходимое поле" name="task_form[name]" id="name" class="form-control input-lg" placeholder="Имя" value="<?= $name ?>" required>
                        <div class="help-block with-errors"><?= $name_err ?></div>
                    </div>
                    <div class="form-group <?= $is_send && isset($email_err) ? 'has-error' : ''?>">
                        <input type="email" data-error="Неверная почта" name="task_form[email]" id="email" class="form-control input-lg" placeholder="Почта" value="<?= $email ?>" required>
                        <div class="help-block with-errors"><?= $email_err ?></div>
                    </div>
                    <div class="form-group <?= $is_send && isset($text_err) ? 'has-error' : ''?>">
                        <textarea class="form-control" data-error="Необходимое поле" name="task_form[text]" id="description" rows="5" placeholder="Задача" required><?= $text ?></textarea>
                        <div class="help-block with-errors"><?= $text_err ?></div>
                    </div>
                    <div>
                        <input type="submit" class="btn btn-md" value="Создать">
                        <a class="login-detail-panel-button btn pull-right" href="/" >
                            Вернуться
                        </a>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
</body>
</html>
