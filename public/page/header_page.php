<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/public/css/bootstrap.min.css" id="bootstrap-css">
    <link rel="stylesheet" href="/public/css/style.css">
    <script src="/public/js/jquery.min.js"></script>
    <script src="/public/js/bootstrap.min.js"></script>
    <script src="/public/js/validator.min.js"></script>
    <title>Task is my life</title>
</head>
