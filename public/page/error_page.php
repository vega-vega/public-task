<!DOCTYPE html>
    <?php include 'public/page/header_page.php'; ?>
<body>
<div class="container">
    <div class="error">
        <div class="error-code m-b-10 m-t-20">404 <i class="fa fa-warning"></i></div>
        <h3 class="font-bold">Мы не смогли найти страницу..</h3>

        <div class="error-desc">
            Извините, но искомая страница не найдена или не существует.
            <br/> Пожалуйста нажмите кнопку ниже, чтобы вернуться на главную страницу.
            <div>
                <a class="login-detail-panel-button btn" href="/" >
                    <i class="fa fa-arrow-left"></i>
                    Вернуться на главную страницу
                </a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
