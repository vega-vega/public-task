<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Task is my life</a>
        </div>
        <?php
            if (isset($response)) {
        ?>
            <ul class="nav navbar-nav navbar-right">
                <?php
                    if ($response->isIsAuth()) {
                ?>
                    <li><a href="/logout">Выйти</a></li>
                <?php
                    } else {
                ?>
                    <li><a href="/login">Войти</a></li>
                <?php
                    }
                ?>
            </ul>
        <?php
            }
        ?>
    </div>
</nav>

