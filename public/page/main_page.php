<!DOCTYPE html>
<?php include 'public/page/header_page.php'; ?>
<body>
<div class="container">
    <?php include 'public/page/navigation_page.php'; ?>
    <div class="row">
        <div class="col-md-12 text-center">
            <p><a class="btn btn-lg btn-success" href="/task" role="button">Добавить задачу</a></p>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Сортировать по:</a>
                </div>
                <ul class="nav navbar-nav">
                    <?php
                        $routes = explode('/', $_SERVER['REQUEST_URI']);
                        $path = $routes[1] != '' ? $routes[1] : '1';
                        $sort = isset($routes[2]) ? $routes[2] : '';
                        $name = $path . '/' . ($sort == 'name' ? 'name_desc' : 'name');
                        $email = $path . '/' . ($sort == 'email' ? 'email_desc' : 'email');
                        $status = $path . '/' . ($sort == 'status' ? 'status_desc' : 'status');
                    ?>
                    <li><a href="/<?= $name ?>">Имени</a></li>
                    <li><a href="/<?= $email ?>">Почте</a></li>
                    <li><a href="/<?= $status ?>">Статусу</a></li>
                </ul>
            </div>
        </div>
    </div>
    <hr>
    <?php
        foreach ($response->getTaskArr() as $task) {
    ?>
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <div class="row">
                    <div class="col-md-3">
                        <b><?= $task->getName() ?></b>
                    </div>
                    <div class="col-md-4">
                        <i><?= $task->getFix() == 1 ? "Отредактировано администратором" : "" ?></i>
                    </div>
                    <div class="col-md-4 text-right">
                        <p><?= $task->getStatus() == 1 ? "Выполено" : "Не выполнено" ?></p>
                    </div>
                </div>
                <p><?= $task->getEmail() ?></p>
                <p><?= $task->getText() ?> </p>
            </div>
        </div>
        <?php
            if ($response->isIsAuth()) {
        ?>
            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <a class="btn btn-default" href="/done/<?= $task->getId() . '/' . $path?>" role="button">Выполнено</a>
                        </div>
                        <div class="col-md-6">
                            <form action="/task" method="post">
                                <button type="submit" name="task_edit[id]" value="<?= $task->getId() ?>" class="btn btn-default">Редактировать</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?php
            }
        ?>
        <hr>
    <?php
        }
    ?>
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <?php
                for ($i = 1; $i <= $response->getTaskCount(); $i++) {
                $link = $i . "/" . (isset($routes[2]) ? $routes[2] : '');
            ?>
                    <li class="page-item"><a class="page-link" href="/<?= $link ?>"><?= $i ?></a></li>
            <?php
                }
            ?>
        </ul>
    </nav>
</div>
</body>
</html>
