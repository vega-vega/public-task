<!DOCTYPE html>
<?php include 'public/page/header_page.php'; ?>
<body>
<div class="container">
    <?php include 'public/page/navigation_page.php'; ?>
    <?php

    if (count($response->getError()) > 0) {
        ?>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="alert alert-danger" role="alert">
                    <?= $response->getError()[0] ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="row">
        <div class= "col-md-offset-4 col-md-4">
            <form role="form" method="post">
                <fieldset>
                    <p class="text-uppercase">Авторазиция</p>

                    <div class="form-group">
                        <input type="text" name="authorization[login]" id="login" class="form-control input-lg" placeholder="Имя">
                    </div>
                    <div class="form-group">
                        <input type="password" name="authorization[password]" id="password" class="form-control input-lg" placeholder="Пароль">
                    </div>
                    <div>
                        <input type="submit" class="btn btn-md" value="Войти">
                        <a class="login-detail-panel-button btn pull-right" href="/" >
                            Вернуться
                        </a>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
</body>
</html>
