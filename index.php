<?php

ini_set('display_errors', 1);
require_once 'src/model/Task.php';
require_once 'src/model/Task_Response.php';
require_once 'src/View.php';
require_once 'src/Manager.php';
require_once 'src/Controller.php';
require_once 'src/Route.php';
session_start();

Route::start();